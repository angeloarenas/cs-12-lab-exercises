#include <string>
#include <fstream>

using namespace std;
class HTMLWriter {
	
	//operator<<(ostream&, 
	
	private:
		ofstream fout;
		string filename;
		enum styles {
			center, underlined, bold, italic
		};
		
	public:
		HTMLWriter();
		HTMLWriter(string);
		void start_table();
		void start_table_row();
		void end_table_row();
		void start_table_entry(string);
		void end_table();
		void begin();
		void end();
		void open();
		void title();
};
