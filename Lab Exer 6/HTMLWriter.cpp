#include <iostream>
#include <string>
#include <fstream>
#include "HTMLWriter.h"
using namespace std;

HTMLWriter::HTMLWriter() {
	filename = "1.html";
	fout.open(filename.c_str());
	fout << "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
}

HTMLWriter::HTMLWriter(string infilename) : filename(infilename){
	fout.open(filename.c_str());
	fout << "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
}

void HTMLWriter::begin() {
	fout << "<html>";
}

void HTMLWriter::end() {
	fout << "</html>";
	fout.close();
}

void HTMLWriter::start_table() {
	fout << "<table>\n";
}

void HTMLWriter::title() {
	fout << "<head>\n";
	fout << "<title>HTML Writer</title>\n";
	fout << "</head>\n";
}

void HTMLWriter::start_table_row() {
	fout << "<tr>\n";
}

void HTMLWriter::start_table_entry(string input) {
	fout << "<th>" << input << "</th>";
}

void HTMLWriter::end_table_row() {
	fout << "</tr>\n";
}

void HTMLWriter::end_table() {
	fout << "</table>\n";
}
