#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "HTMLWriter.h"
using namespace std;

//Define for styles

int main(int argc, char* argv[]) {
	HTMLWriter htmlwriter("GWA.html");
	ifstream fin;
	//Parsing
	if (argc > 1)
		fin.open(argv[1]);
	else {
		string filename;
		cout << "Enter csv filename: ";
		cin >> (filename);
		fin.open(filename.c_str());	//delete filename;
	}	
	
	if (!fin.fail()) {
		htmlwriter.start_table();
		while(!fin.eof()) {
			string line;
			getline(fin,line);
			istringstream iss(line);
			htmlwriter.start_table_row();
			while(!iss.eof()) {
				string entry;
				getline(iss, entry, ',');
				htmlwriter.start_table_entry(entry);
			}
			htmlwriter.end_table_row();
		}
		htmlwriter.end_table();
		cout << "Finished writing table\n";
	}
	else
		cout << "CSV file not found\n";
}
