/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
*/
class Time {
	private:		
		int hour, min, sec;
		bool am_pm;		//true for am, false for pm
		bool format;		//true = 12-hour, false = 24
	public:
		void set();
		void setFormat();
		void display();	
		bool tic();		//returns true if day is finished
};
