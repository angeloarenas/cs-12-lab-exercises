/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
*/
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include "time.h"

using namespace std;

void Time::set() {
	cout << "Input hour (integer): ";
	cin >> hour;
	cout << "Input minute (integer): ";
	cin >> min;	
	cout << "Input second (integer): ";
	cin >> sec;
	if (format) {
		cout << "Input 1 for AM, 0 for PM: ";
		cin >> am_pm;
	}
}

void Time::setFormat() {
	cout << "Enter 1 for 12-hour, 0 for 24-hour: ";
	cin >> format;
}

void Time::display () {
	if (format) {
		if (am_pm)
			cout << setw(2) << setfill('0') << hour << ":" 
			<< setw(2) << setfill('0') << min << ":" 
			<< setw(2) << setfill('0') << sec << " AM";
		else
			cout << setw(2) << setfill('0') << hour << ":" 
			<< setw(2) << setfill('0') << min << ":" 
			<< setw(2) << setfill('0') << sec << " PM";
	} 
	else 
		cout << setw(2) << setfill('0') << hour << ":" 
		<< setw(2) << setfill('0') << min << ":" 
		<< setw(2) << setfill('0') << sec;
	cout << endl;
}

bool Time::tic() {
	sleep(1);
	if (sec >= 59) {
		sec = 0;
		if (min >= 59) {
			min = 0;
			if (hour >= 23 && !format) {
				hour = 0;
				return true;
			}
			else if (hour == 11 && format) {
				hour++;
				if (am_pm)
					am_pm = false;
				else {
					am_pm = true;
					return true;
				}
			}
			else if (hour == 12 && format)
				hour = 1;
			else hour++;
		}
		else min++;
	}
	else sec++;
	return false;
}
