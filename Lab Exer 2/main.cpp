/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano

NOTE: Program compiled and tested using Ubuntu 
Please remove system("clear") and sleep() if there are any errors
*/
#include <iostream>
#include <stdlib.h>
#include "time.h"
#include "date.h"

using namespace std;

void displaySyncDateAndTime(Time&, Date&);

int main() {
	Date today, birthday;
	Time time;
	
	system("clear");
	cout << "Please input values correctly to avoid problems\n";
	cout << "Set time format\n";
	time.setFormat();
	cout << "Set time\n";
	time.set();
	cout << "Set date today\n";
	today.set();
	cout << "Set your Birthday\n";
	birthday.set();
	
	while (true) {
		if (today.isSameDay(birthday))
			cout << "Happy Birthday!\n";
		displaySyncDateAndTime(time,today);
		system("clear");
	}
}

void displaySyncDateAndTime(Time& time, Date& date) {
	date.display();
	time.display();	
	if (time.tic()) {
		date.next();
	}
}
