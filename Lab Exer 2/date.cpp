/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
*/
#include <iostream>
#include "date.h"

using namespace std;

const char monthChar[][10] = {{"January"},{"February"},{"March"},
				{"April"},{"May"},{"June"},
				{"July"},{"August"},{"September"},
				{"October"},{"November"},{"December"}};
const char dayChar[][10] = {{"Sunday"}, {"Monday"}, {"Tuesday"}, {"Wednesday"},
				{"Thursday"}, {"Friday"}, {"Saturday"}};

void Date::set() {
	cout << "Enter month (integer [1-12]): ";
	cin >> month;
	cout << "Enter day (integer [1-31] depending on month): ";
	cin >> day;
	cout << "Enter year (integer): ";
	cin >> year;
}

void Date::display() {
	cout << dayChar[dayofWeek()] << endl;
	cout << monthChar[month-1] << " " << day << ", " << year << endl;
}

int Date::dayofWeek() {
	//Zeller's

	int a, b, c, d, w, x, y, z, r;
	int newyear;
	if (month == 1 || month == 2) {
		a = month+10;
		newyear = year - 1;
	}
	else {
		a = month-2;
		newyear = year;
	}

	b = day;
	c = newyear%100;
	d = newyear/100;
	w = (13*a-1)/5;
	x = c/4;
	y = d/4;
	z = w+x+y+b+c-2*d;
	r = z%7;
	if (r<0)
		r += 7;
	return r;
}

bool Date::isLeapYear() {
	if (year%400 == 0 || (year%400 != 0 && year%100 != 0 && year%4 == 0))
		return true;
	else
		return false;
}

void Date::prev() {
	day--;
}

void Date::next() {
	int lastdayofmonth;
	
	if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month  == 12) 
		lastdayofmonth = 31;
	else if (month == 2)  {
		if (isLeapYear())
			lastdayofmonth = 29;
		else 
			lastdayofmonth = 28;
	}
	else 
		lastdayofmonth = 30;
	if (day >= lastdayofmonth) {
		day = 1;
		if (month < 12)
			month++;
		else {
			month = 1;
			year++;
		}
	}
	else
		day++;
}

bool Date::isSameDay(Date birthday) {
	if (birthday.day == day && birthday.month == month) {
		return true;
	}
	else 
		return false;
}
