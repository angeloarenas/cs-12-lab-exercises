/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
*/
class Date {
	private:		
		int day, month, year;
		int dayofWeek();	//Used only inside
		bool isLeapYear();	//Used only inside
	public:
		void set();
		void display();
		void prev();
		void next();
		bool isSameDay(Date);
};
