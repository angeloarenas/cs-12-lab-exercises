/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
Lab Exercise 3: 2D Cellular Automata
*/

#include "ca.h"
#include <iostream>

using namespace std;

CA::CA(int in_size, int in_rule_no) {
	gen = 0;
	size = in_size;		
	rule_no = in_rule_no;
	
	//Init binary rule array
	for (int i = 0; i < 10; i++)
		rule[i] = false;
	decode_rule();	

	//Allocate dynamic pointer array
	current_state = new bool*[size];
	for (int i = 0; i < size; i++) 
		current_state[i] = new bool[size];
	
	prev_state = new bool*[size];
	for (int i = 0; i < size; i++) 
		prev_state[i] = new bool[size];
	
	//Set seed on middle
	current_state[size/2][size/2] = true; 	
}

void CA::evolve() {
	int neighbors_on = 0;
	gen++;
	//Prev = Current
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {	
			prev_state[i][j] = current_state[i][j];
		}
	}
	//Apply Rule
	//Excluded boundaries
	for (int i = 1; i < size-1; i++) {
		for (int j = 1; j < size-1; j++) {	
			if (prev_state[i][j+1])
				neighbors_on++;
			if (prev_state[i][j-1])
				neighbors_on++;
			if (prev_state[i+1][j])
				neighbors_on++;
			if (prev_state[i-1][j])
				neighbors_on++;
			current_state[i][j] = rule[9-(neighbors_on*2+prev_state[i][j])];
			neighbors_on = 0;
		}
	}
}

void CA::display() {
	for (int i = 0; i < size; i++) {
		cout << '\n';
		for (int j = 0; j < size; j++) 
			current_state[i][j] ? cout << "1": cout << " ";	
	}
	cout << "\nGen: " << gen << "\t\tRule: " << rule_no << "\tDecoded Rule: ";
	for (int i = 0; i < 10; i++)
		rule[i] ? cout << 1: cout << "0";
	cout << endl;
}

void CA::decode_rule() {
	int newrule = rule_no;
	for (int i = 9; i >= 0; i--) {
		rule[i] = newrule%2;
		newrule = newrule/2;
	}
}

CA::~CA() {
	//Delete pointer array
	for (int i = 0; i < size; i++) 
		delete [] current_state[i];
	delete [] current_state;
	
	for (int i = 0; i < size; i++) 
		delete [] prev_state[i];
	delete [] prev_state;
}
