/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
Lab Exercise 3: 2D Cellular Automata
*/

class CA {
	private:
		int size;
		bool** current_state;
		bool** prev_state;
		int rule_no;
		bool rule[10];
		int gen;

		void decode_rule();	//Utility function
	public:
		CA(int, int);
		~CA();
		void evolve();
		void display();
};
