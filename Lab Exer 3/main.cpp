/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
Lab Exercise 3: 2D Cellular Automata

	Using size 50
	Rules:
		421: Square with diamond with square
		974: Diamond snowflake
		910: Cross 
		412: Just Diamond
		918: Diamond with cross
		823: Squares inside square

Note: If grid is not visible try zooming out
*/

#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include "ca.h"

using namespace std;

int main() {
	int size = 25;
	int rule_no;
	
	cout << "Enter size: \n";
	cin >> size;
	cout << "Enter rule no.: \n";
	cin >> rule_no;		//Input must be < 1024
	CA ca(size, rule_no);
	while (true) {
		ca.display();
		ca.evolve();
		usleep(1000 * 500);
		system("clear");
	}
}
