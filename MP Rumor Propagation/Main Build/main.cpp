#include <iostream>
#include <string>
#include "SmallWorld.h"
using namespace std;

int main() {
	int n = 1000;
	int k = 10;
	double pworld = 0.1;
	string R = "";
	int q = 64;
	for (int i = 0; i < q; i++)
		R += "0";
	int T = 1000;
	double prumor = 0.5;
	string filename = "avgdiff.txt";

	SmallWorld smallworld(n,k,pworld,SmallWorld::CASE1);
	smallworld.start_regularconnect();
	smallworld.start_rewire();
		
	smallworld.start_rumorpropagation(R, q, T, prumor,filename);
}

/*
Flow:
create
regular connect
rewire
rumor propagation
*/
