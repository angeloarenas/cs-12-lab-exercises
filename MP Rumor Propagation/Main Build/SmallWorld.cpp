#include "SmallWorld.h"
#include <iostream>
#include "randgen.h"
using namespace std;

SmallWorld::SmallWorld(int n_nodes, int k_edges, double pr, PropCase in_propcase) 
	: n(n_nodes), k(k_edges), p(pr),  propcase(in_propcase) {
	node = new Node[n];
	for (int i = 0; i < n; i++) {
		node[i].init(i);
	}
}

void SmallWorld::print_neighborhood() {
	for (int i = 0; i < n; i++) { 
		node[i].print_neighbors();
	}
}

void SmallWorld::start_regularconnect() {
	int b;
	//Cannot connect simultaneous because it will be hard to find which to remove when rewiring
	//Outgoing first
	for (int i = 0; i < k/2; i++) { //For each "edge" or "connection" 
		for (int j = 0; j < n; j++) {	//For each node
			if (i+j+1 >= n) 
				b=i+j-n+1;
			else
				b=j+i+1;
			node[j].add_neighbor(b);
		}
	}
	//Incoming
	for (int i = 0; i < k/2; i++) { //For each "edge" or "connection" 
		for (int j = 0; j < n; j++) {	//For each node
			if (i+j+1 >= n) 
				b=i+j-n+1;
			else
				b=j+i+1;
			node[b].add_neighbor(j);
		}
	}
	cout << "Finished Regular Connect\n";
	print_neighborhood();
}

void SmallWorld::start_rewire() {
	for (int i = 0; i < k/2; i++) { 
		for (int j = 0; j < n; j++) {
			if (random.integer(100)<p*100) {
				cout << "rewire " << i << ' ' << j << '\n';
				int randomnode;
				do {
					randomnode = random.integer(n-1);
				} while (node[j].has_neighbor(randomnode));
				rewire(i,node[j],node[randomnode],node[node[j].get_neighbor(i)]);
				cout << "rewired " << j << " to " << randomnode << '\n';
			}
		}	
	}
	print_neighborhood();
}

void SmallWorld::start_rumorpropagation(string inputR, int inputQ, int inputT, double inputPR, string filename) {
	fout.open(filename.c_str());
	R = inputR;	
	q = inputQ;
	T = inputT;	
	pr = inputPR;
	
	int randomnode = random.integer(n-1);
	node[randomnode].init_rumor(R);
	int randneighbor;
	double averagediff;
	// Runs T*j^2
	for (int i = 0; i < T; i++) {
		for (int j = 0; j < n; j++) {
			if (node[j].has_heard()) {
				randneighbor = node[j].get_randneighbor();
				if (node[randneighbor].has_heard()) {
					switch (propcase) {
						case CASE1:
							interact(node[j],node[randneighbor],Node::DISAGREE);
						break;
						case CASE2:
							interact(node[j],node[randneighbor],Node::AGREE);
						break;
						case CASE12:
							if (random.integer(1)) 
								interact(node[j],node[randneighbor],Node::AGREE);
							else
								interact(node[j],node[randneighbor],Node::DISAGREE);
						break;
					}
				}
				else
					interact(node[j],node[randneighbor],Node::NOTHEARD);
			}	
		}
		averagediff = 0;
		for (int j = 0; j < n; j++) {
			if (node[j].has_heard() && random.integer(100)<pr*100) {
				node[j].randomizerumor();
			}	
			averagediff += node[j].calc_difference();
		}
		averagediff/=n;
		//cout << averagediff << endl;
		fout << averagediff << endl;
	}
	/*
	for (int j = 0; j < n; j++) {
		if (node[j].has_heard()) {
			node[j].say_rumor();
		}	
	}
	*/
	fout.close();
	cout << "Finished writing average difference - " << filename << endl;
}

SmallWorld::~SmallWorld() {
	delete[] node;
}
