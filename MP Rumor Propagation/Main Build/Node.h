#include "Rumor.h"
#include <vector>
#include "randgen.h"

class Node {

	private:
		Random random;
		int ID;
		vector<int> neighbors;
		
		bool hasheard;
		Rumor rumor;
	public:
		enum Interaction {AGREE, DISAGREE, NOTHEARD};
		Node();
		void init(int);
		bool has_neighbor(int);
		void add_neighbor(int);
		void print_neighbors();
		int get_neighbor(int);
		int get_randneighbor();
		
		void init_rumor(string);
		bool has_heard();
		void randomizerumor();
		void say_rumor();
		int calc_difference();
		
	friend void rewire(int, Node&, Node&, Node&);
	friend void interact(Node&, Node&, Interaction);
};
