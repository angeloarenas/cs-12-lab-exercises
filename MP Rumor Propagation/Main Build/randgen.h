#ifndef RANDGEN_H
#define RANDGEN_H

#include<iostream>
#include<cassert>
#include<cstdlib>
#include<ctime>
#include<vector>
using namespace std;

class Random
{
	size_t seed;
	void random();
public:
	Random(size_t=0);
	size_t integer(size_t=RAND_MAX, size_t=0);
	double real(double=1.0, double=0.0);
	template <class T>
	T choose(const T*, int);
	void random_order(int*, int);
};
#endif
