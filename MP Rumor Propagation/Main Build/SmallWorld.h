#include "Node.h"
#include <string>
#include <vector>
#include "randgen.h"
#include <fstream>
using namespace std;

class SmallWorld {
	public:
		enum PropCase {CASE1, CASE2, CASE12};
		SmallWorld(int,int,double,PropCase);
		~SmallWorld();
		void print_neighborhood();
		void start_regularconnect();
		void start_rewire();
		
		void start_rumorpropagation(string, int, int, double, string);
	private:
		PropCase propcase;
		ofstream fout;
		Random random;

		Node* node;
		int n;		//No. of nodes
		int k;		//No. of edges per node
		double p;	//Probability of rewiring

		string R;	//Rumor string
		int q;		//Length of rumor
		int T;		//Iterations
		double pr;	//Probability of alteration
};

