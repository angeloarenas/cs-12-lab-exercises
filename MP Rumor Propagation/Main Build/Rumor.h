#include <string>
using namespace std;

class Rumor{
	private:
		string strumor;	
	public:
		void set_rumor(string);
		string get_rumor();
		int get_q();
		void alterbit(int);
		int calc_difference();
};
