#include "Rumor.h"

void Rumor::set_rumor(string input) {
	strumor = input;
}

string Rumor::get_rumor() {
	return strumor;
}	

int Rumor::get_q() {
	return strumor.size();
}

void Rumor::alterbit(int i) {
	strumor[i]=='0' ? strumor[i]='1':strumor[i]='0';
}

int Rumor::calc_difference() {
	int count = 0;
	for (int i = 0; i < strumor.size(); i++)
		if (strumor[i] == '1')
			count++;
	return count;
}
