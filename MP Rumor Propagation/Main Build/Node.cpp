#include "Node.h"
#include <iostream>
#include "randgen.h"

using namespace std;

Node::Node() {
	hasheard = false;
}

void Node::init(int inputID) {
	ID = inputID;
}

bool Node::has_neighbor(int a) {
	if (ID == a)
		return true;
	for (int i = 0; i < neighbors.size(); i++)
		if (neighbors[i] == a)
			return true;
	return false;
}

void rewire(int i, Node& a, Node& b, Node& c) {
	a.neighbors[i] = b.ID;
	b.neighbors.push_back(a.ID);
	for (int x = 0; x < c.neighbors.size(); x++)
		if (c.neighbors[x] == a.ID)
			c.neighbors.erase(c.neighbors.begin()+x);
}

void Node::add_neighbor(int a) {
	neighbors.push_back(a);
}

void Node::print_neighbors() {
	cout << "Node " << ID << '\t';
	for (int i = 0; i < neighbors.size(); i++)
		cout << neighbors[i] << ' ';
	cout << endl;
}

int Node::get_neighbor(int a) {
	return neighbors[a];
}

void Node::init_rumor(string input) {
	rumor.set_rumor(input);
	hasheard = true;
	//cout << "Node " << ID << '\t' << input << endl;
}

int Node::get_randneighbor() {
	return random.choose(&neighbors[0],neighbors.size());
	//from here: http://stackoverflow.com/questions/2923272/how-to-convert-vector-to-array-c
}

void interact(Node& a, Node& b, Node::Interaction interaction) {
	switch (interaction) {
		case Node::AGREE:
			//cout << "AGREE\n";
			if (a.random.integer(1))
				b.init_rumor(a.rumor.get_rumor());
			else
				a.init_rumor(b.rumor.get_rumor());
		break;
		case Node::DISAGREE:		
			//cout << "DISAGREE\n";
			return;
		break;
		case Node::NOTHEARD:
			//cout << "NOTHEARD\n";
			b.init_rumor(a.rumor.get_rumor());
		break;
	}
}

bool Node::has_heard() {
	return hasheard;
}

void Node::randomizerumor() {
	rumor.alterbit(random.integer(rumor.get_q()-1));
}

void Node::say_rumor() {
	cout << rumor.get_rumor() << endl;
}

int Node::calc_difference() {
	return rumor.calc_difference();
}
