#include <iostream>
#include <cstdlib>

using namespace std;

int main() {
	for (int i = 0; i < 20; i++) {
		bool value = srand((unsigned)time(0))%100 < 10; 
		/*The rand() % 100 will give you a random number between 0 and 100, 
		and the probability of it being under 75 is, well, 75%. You can substitute the 75 for any probability you want.
		*/
		cout << value << '\n';
	}
}
