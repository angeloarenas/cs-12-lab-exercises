#include "SmallWorld.h"
#include <iostream>
#include "randgen.cpp"
using namespace std;

void SmallWorld::create(int n_nodes, int k_edges, double pr) {
	n = n_nodes;
	k = k_edges;
	p = pr;
	node = new Node[n];
	mainconnectionarray = new int*[k/2];
	for (int i = 0; i < k/2; i++) {
		mainconnectionarray[i] = new int[n];	
	}
}

void SmallWorld::regularconnect() {
	int b;
	for (int i = 0; i < k/2; i++) { //For each "edge" or "connection" 
		for (int j = 0; j < n; j++) {	//For each node
			if (i+j+1 >= n) 
				b=i+j-n+1;
			else
				b=j+i+1;
			connectnode(i,j,b);
		}
	}
	initneighbors();
	printconnections();
}

void SmallWorld::rewire() {
	Random random;
	for (int i = 0; i < k/2; i++) { 
		for (int j = 0; j < n; j++) {
			if (random.integer(100)<p*100) {
				cout << "rewire " << i << ' ' << j << '\n';
				int randomnode;
				do {
					randomnode = random.integer(n-1);
				} while (duplicateedge(j, randomnode));
				connectnode(i,j,randomnode);
				cout << "rewired " << j << " to " << randomnode << '\n';
			}
		}	
	}
	initneighbors();
	printconnections();
}

void SmallWorld::initneighbors() {
	//THIS PROCESS TAKES 2*n^2*k/2
	for (int i = 0; i < n; i++) {
		int count = 0;
		for (int x = 0; x < k/2; x++) {	
			for (int y = 0; y < n; y++) {
				if (mainconnectionarray[x][y] == i)
					count++;
			}
		}
		count += k/2;
		
		node[i].initneighborhood(count);
		for (int x = 0; x < k/2; x++) {
			node[i].addneighbor(mainconnectionarray[x][i]);
			for (int y = 0; y < n; y++) {
				if (mainconnectionarray[x][y] == i)
					node[i].addneighbor(y);
			}
		}
		node[i].printneighbors();
	}
}

void SmallWorld::connectnode(int i, int a, int b) {
	mainconnectionarray[i][a] = b;
}

bool SmallWorld::duplicateedge(int a, int b) {
	if (a == b) {
		cout << "SAME EDGE " << a << ' ' << b << '\n';
		return true;
	}
	for (int i = 0; i < k/2; i++) {
		if (mainconnectionarray[i][a] == b || mainconnectionarray[i][b] == a ) {
			cout << "Duplicate edge " << i << ' ' << a << ' ' << b << '\n';			
			return true;
		}
	}
	return false;
}

void SmallWorld::printconnections() {
	for (int i = 0; i < k/2; i++) { 
		for (int j = 0; j < n; j++) {
			cout << j << " " << mainconnectionarray[i][j]  << endl;
		}
	}
}

SmallWorld::~SmallWorld() {
	delete[] node;
	for (int i = 0; i < k/2; i++) {
		delete[] mainconnectionarray[i];
	}
	delete[] mainconnectionarray;	
}
