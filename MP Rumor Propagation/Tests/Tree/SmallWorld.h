#include "Node.h"

#include <string>

using namespace std;

class SmallWorld {
	private:
		int** mainconnectionarray;
		Node* node;
		int n;		//No. of nodes
		int k;		//No. of edges per node
		double p;	//Probability of rewiring
	public:
		void connectnode(int,int,int);
		void create(int, int, double);	
		void regularconnect();
		void rewire();
		void initneighbors();

		bool duplicateedge(int,int);	//private
		void printconnections();

		void startRumor(string, int, int);
		void averagedifference();
		~SmallWorld();
};
