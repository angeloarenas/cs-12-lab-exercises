#ifndef GUI_H
#define GUI_H

#include<iostream>
#include<cassert>
#include<cstdlib>
#include<ctime>
#include<vector>
using namespace std;

class Random
{
	size_t seed;
	void random();
public:
	Random(size_t);
	size_t integer(size_t, size_t);
	double real(double, double);
	template <class T>
	T choose(const T*, int);
	void random_order(int*, int);
};

void Random::random()
{
    seed=(1103515245*seed+123456789)%RAND_MAX;
}

Random::Random(size_t inputseed=0)
{
    if (inputseed>0)
        seed=inputseed;
    else
        seed=time(NULL);
    random();
}

size_t Random::integer(size_t upperbound=RAND_MAX, size_t lowerbound=0)
{
    random();
    return seed/10 % (upperbound-lowerbound+1)+lowerbound;
}

double Random::real(double upperbound=1.0, double lowerbound=0.0)
{
    random();
    return lowerbound+(upperbound-lowerbound)*seed/RAND_MAX;
}

template <class T>
T Random::choose(const T* A, int n)
{
    return *(A+integer(n-1));
}

void Random::random_order(int* order, int n)
{
    vector<int> index(n);
    for (int i=0; i<n; i++)
        index[i]=i;
    for (int i=0; i<n; i++)
    {
        int x=integer(n-i-1);
        order[i]=index[x];
        index.erase(index.begin()+x);
    }
}

/*
void example()
{
	//USAGE
	//Random dice(123456789); //sets seed to 123456789
	Random dice;	//sets random seed using computer time
	cout<<"Real number in [0, 1] \t\t\t"<<dice.real()<<endl;
	cout<<"Real number in [0, 10] \t\t\t"<<dice.real(10)<<endl;
	cout<<"Real number in [5, 10] \t\t\t"<<dice.real(10,5)<<endl;
	cout<<"Integer number in [0, RAND_MAX] \t"<<dice.integer()<<endl;
	cout<<"Integer number in [0, 10] \t\t"<<dice.integer(10)<<endl;
	cout<<"Integer number in [5, 10] \t\t"<<dice.integer(10,5)<<endl;
	int A[]={12, 0, 15, 1, 8, 2};
	cout<<"Element in [12 0 15 1 8 2] \t\t"<<dice.choose(A,6)<<endl;
	cout<<"Creating a random ordering of \n{0 1 2 3 4 5 6 7 8 9}\t\t\t";
	int n=10;
	int* order=new int[n];
    dice.random_order(order, n);
    for (int i=0; i<n; i++)
        cout<<order[i]<<" ";
    cout<<endl;
    //do not forget to deallocate
    delete []order;
}
*/

#endif
