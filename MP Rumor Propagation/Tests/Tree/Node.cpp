#include "Node.h"
#include <iostream>

using namespace std;

Node::Node() {
}

Node::Node(int inputID) :
	ID(inputID) {}

Node::~Node() {
	delete[] neighbors;
}

void Node::initneighborhood(int inputcount) {
	if (neighbors != NULL)
		delete[] neighbors;
	totalneighbors = inputcount;
	neighbors = new int[totalneighbors];
	neighborcount = 0;
}

void Node::addneighbor(int inputID) {
	neighbors[neighborcount] = inputID;
	neighborcount++;
}

void Node::printneighbors() {
	cout << "Total Neighbors: " << totalneighbors << '\t';
	for (int i = 0; i < totalneighbors; i++)
		cout << neighbors[i] << ' ';
	cout << endl;
}
