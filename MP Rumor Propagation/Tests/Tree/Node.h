#include "Rumor.h"
class Node{

	friend void rumorinteract(Node, Node);
	friend double comparerumor(Node, Node); 

	private:
		Rumor rumor;
		int ID;
		int totalneighbors;
		int neighborcount;
		int* neighbors;
		
	public:
		Node();
		Node(int);	
		~Node();
		void initneighborhood(int);
		void addneighbor(int);
		void printneighbors();
};
