#include <iostream>
#include "SmallWorld.h"
using namespace std;

int main() {
	SmallWorld smallworld;
	smallworld.create(1000,10,.1);
	smallworld.regularconnect();
	smallworld.rewire();
	char a;
	cin >> a;
}

/*
Flow:
create
regular connect
rewire
create neighborhood array
*/
