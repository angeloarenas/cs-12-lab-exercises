#include "InputGUI.h"
#include <iostream>

using namespace std;

InputGUI::InputGUI(int x, int y, string input, Type type, bool frameOnOff)
	: GUI(x, y, frameOnOff), Input(type, input) {
}
