#include <iostream>
#include "InputGUI.h"
#include "OutputGUI.h"
using namespace std;

int main() {
	string name;
	InputGUI inputGUI(10,11,"Enter name: ",InputGUI::STRING, 1);
	inputGUI >> name;
	OutputGUI outputGUI(20,11,1);
	outputGUI << "Hello " << name << "!\n";
	//DESTRUCTOR RUNS - CLEARS SCREEN - SCROLL UP TO SEE OUTPUT
}

