#include "Input.h"
#include <string>
#include <iostream>

using namespace std;

Input::Input(Type type, string input)
	: inputType(type), message(input) {	
	cout << message;
	do {
		switch(inputType) {
		case INTEGER:
			cin >> intinput;
		break;
		case DOUBLE:
			cin >> doubleinput;
		break;
		case STRING:
			cin >> stringinput;
		break;
		}
	} while(!validate(cin));
}

bool Input::validate(istream& in) {
	if (in.fail()) {
		in.clear();
		in.ignore(1000,'\n');
		cout << "ERROR INPUT";
		return false;
	}
	return true;
}

Input& operator>>(Input& input, int& assign) {
	assign = input.intinput;	
	return input;
}

Input& operator>>(Input& input, double& assign) {
	assign = input.doubleinput;	
	return input;
}

Input& operator>>(Input& input, string& assign) {
	assign = input.stringinput;	
	return input;
}

