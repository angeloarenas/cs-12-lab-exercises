#ifndef GUI_H
#define GUI_H

class GUI {
	private:
		int x, y;
		bool frameOn;
	public:
		GUI(int, int, bool);
		~GUI();
		void gotoxy();
		void drawFrame();
};

#endif

