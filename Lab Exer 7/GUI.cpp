#include "GUI.h"
#include <iostream>
#include <stdlib.h>
using namespace std;

GUI::GUI(int inx, int iny, bool inframe)
	: x(inx), y(iny), frameOn(inframe) {
	gotoxy();
}

GUI::~GUI() {
	system("clear");
	x = 0;
	y = 0;
	gotoxy();
}

void GUI::gotoxy() {
	cout << "\033[" << x << ';' << y << "H";
}

void GUI::drawFrame() {
}
