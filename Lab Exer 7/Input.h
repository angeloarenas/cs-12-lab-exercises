#include <string>
using namespace std;
class Input {
	friend Input& operator>>(Input&, int&);
	friend Input& operator>>(Input&, double&);
	friend Input& operator>>(Input&, string&);
			
	public:
		enum Type {INTEGER, DOUBLE, STRING};
		Input(Type, string);
		bool validate(istream&);
		
	private:
		string message;
		Type inputType;
		string stringinput;
		double doubleinput;
		int intinput;		
};
