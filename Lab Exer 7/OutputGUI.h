#include <string>
#include "GUI.h"
using namespace std;

class OutputGUI : public GUI {
	friend OutputGUI& operator<<(OutputGUI&, int);
	friend OutputGUI& operator<<(OutputGUI&, double);
	friend OutputGUI& operator<<(OutputGUI&, string);

	public:
		OutputGUI(int, int, bool);
};
