#include "OutputGUI.h"
#include <iostream>
#include <string>

using namespace std;

OutputGUI::OutputGUI(int inx, int iny, bool inframe)
	: GUI(inx, iny, inframe) {
}

OutputGUI& operator<<(OutputGUI& outputGUI, int output) {
	cout << output;
	return outputGUI;
}

OutputGUI& operator<<(OutputGUI& outputGUI, double output) {
	cout << output;
	return outputGUI;
}

OutputGUI& operator<<(OutputGUI& outputGUI, string output) {
	cout << output;
	return outputGUI;
}
