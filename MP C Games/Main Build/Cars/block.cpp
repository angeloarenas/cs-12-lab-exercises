#include "block.h"

Block::Block(int inX, int inY, int inmaxY, int inlane)
	: posX(inX), posY(inY), lane(inlane), blockchar('X'), maxY(inmaxY), lanewidth(3) {
}

Block::Block(const Block& block) 
	: posX(block.posX), posY(block.posY), lane(block.lane), 
	blockchar(block.blockchar), maxY(block.maxY), lanewidth(3){
}

GUI& operator<<(GUI& gui, Block& block) {
	gui.outputxy(block.blockchar,block.posX+block.lane*block.lanewidth,block.posY);
}

void Block::update() {
	posY++;
}

bool Block::atmaxY() {
	return (posY>=maxY);
}

int Block::getlane() {
	return lane;
}
