#include "../GUI.h"

class Car {
	friend GUI& operator<<(GUI&,Car&);
 
	private:
		int posX, posY;	//y not changing
		int lane, lanewidth;
		char carchar;
	public:
		Car(int,int,char);
		void changelane();
		int getlane();
};
