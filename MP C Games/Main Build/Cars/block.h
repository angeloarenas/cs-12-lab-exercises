#include "../GUI.h"

class Block {
	friend GUI& operator<<(GUI&,Block&);
	private:
		int posX, posY, lane, maxY, lanewidth;
		char blockchar;
	public:
		Block(int,int,int,int);
		Block(const Block&);
		void update();
		bool atmaxY();
		int getlane();
};
