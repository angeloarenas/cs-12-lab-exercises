#include "../GUI.h"

class Circle {
	friend GUI& operator<<(GUI&,Circle&);
	private:
		int posX, posY, lane, maxY, lanewidth;
		char circlechar;
	public:
		Circle(int,int,int,int);
		Circle(const Circle&);
		void update();
		bool atmaxY();
		int getlane();
};
