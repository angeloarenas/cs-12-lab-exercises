#include "car.h"
#include "block.h"
#include "circle.h"
#include "../GUI.h"
#include "../Input.h"
#include "../randgen.h"
#include "../highscore.h"
#include "../highscore.cpp"
#include <deque>
#include <pthread.h>

class CarsGame {	
	enum GameState {PLAY, PAUSE, GAMEOVER, END};

	private:
		int sleepTime;
		int elapsedLoop, nextGen;
		int score;
		Random random;
		Highscore<int> highscore;
		GameState gamestate;
		GUI gui;
		Input input;
		pthread_t inputthread;

		Car carA, carB;
		deque<Block> leftblock;
		deque<Block> rightblock;
		deque<Circle> leftcircle;
		deque<Circle> rightcircle;

		int maxY;

		//Util functions
		void update_blockcircle();
		void generate_blockcircle();
		void start_inputthread();
	public:
		CarsGame();
		~CarsGame();
		void newGame();
		void gameLoop();
		void playLoop();
		void pauseLoop();
		void gameoverLoop();
		void gameover();

		void takeinput(char);
};

/*
highscore.cpp must be included because template class cannot be in separate files
*/
