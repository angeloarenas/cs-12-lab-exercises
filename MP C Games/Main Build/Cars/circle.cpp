#include "circle.h"

Circle::Circle(int inX, int inY, int inmaxY, int inlane)
	: posX(inX), posY(inY), lane(inlane), circlechar('O'), maxY(inmaxY), lanewidth(3) {
}

Circle::Circle(const Circle& circle) 
	: posX(circle.posX), posY(circle.posY), lane(circle.lane), 
	circlechar(circle.circlechar), maxY(circle.maxY), lanewidth(3){
}

GUI& operator<<(GUI& gui, Circle& circle) {
	gui.outputxy(circle.circlechar,circle.posX+circle.lane*circle.lanewidth,circle.posY);
}

void Circle::update() {
	posY++;
}

bool Circle::atmaxY() {
	return (posY>=maxY);
}

int Circle::getlane() {
	return lane;
}
