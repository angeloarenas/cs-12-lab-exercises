#include "car.h"

Car::Car(int inX, int inY, char inChar)
: lane(0), posX(inX), posY(inY), carchar(inChar), lanewidth(3) {
}

GUI& operator<<(GUI& gui, Car& car) {
	gui.outputxy(car.carchar,car.posX+car.lane*car.lanewidth,car.posY);
}

void Car::changelane() {
	lane==1?lane=0:lane=1;
}

int Car::getlane() {
	return lane;
}
