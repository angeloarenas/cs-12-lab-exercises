#include "carsgame.h"
#include <iostream>
#include <unistd.h>
using namespace std;

CarsGame::CarsGame() 
	: maxY(30), carA(5,30,'A'), carB(15,30,'B'), sleepTime(50000), 
	elapsedLoop(0), nextGen(10), score(0),
	highscore("carshighscore.txt") {
}

CarsGame::~CarsGame() {
	gui.clearscreen();
	gui.showcursor();
	input.autoin_off();
	cout << "CarsGame destructor called\n";
}

void CarsGame::newGame() {
	gamestate = PLAY;
	gui.hidecursor();
	input.autoin_on();
	start_inputthread();
	gameLoop();
	pthread_join(inputthread,NULL);
}

void CarsGame::gameLoop() {
	score = 0;
	elapsedLoop = 0;
	nextGen = 10;
	while(gamestate!=END) {		
		switch (gamestate) {
			case PLAY:
				playLoop();
			break;
			case PAUSE:
				pauseLoop();
			break;
			case GAMEOVER:
				gameoverLoop();
			break;
		}
		cout.flush();
		usleep(sleepTime);
	}
}

void CarsGame::playLoop() {
	gui.clearscreen();	
	elapsedLoop++;
	if (elapsedLoop >= nextGen) {
		//cout << "GENERATE BLOCK\n";
		nextGen = random.integer(15,6);
		//cout << nextGen;
		elapsedLoop = 0;
		generate_blockcircle();
	}
	update_blockcircle();
	gui << carA;
	gui << carB;
	gui.gotoxy(6,32);
	cout << "Score: " << score;
}

void CarsGame::pauseLoop() {
	gui.outputxy("P A U S E D",6,17);
	gui.outputxy("p to UNPAUSE",4,18);
	gui.outputxy("Press e to EXIT",4,19);
}

void CarsGame::gameoverLoop() {
	gui.outputxy("GAMEOVER",6,17);
	gui.gotoxy(6,18);
	cout << "Your score: " << score;
	gui.gotoxy(6,19);
	cout << "Highscore: " << highscore.get_score();
	gui.outputxy("Press e to EXIT",4,20);
}

void CarsGame::start_inputthread() {
	pthread_create(&inputthread,NULL,input.carsinput,this);
}

void CarsGame::takeinput(char s) {
	switch (s) {
		case 'a':
			carA.changelane();
		break;
		case 'd':
			carB.changelane();
		break;
		case 'p':
			if (gamestate != GAMEOVER)
				gamestate==PLAY?gamestate=PAUSE:gamestate=PLAY;
		break;
		case 'e':
			if (gamestate == PAUSE || gamestate == GAMEOVER) {
				gamestate = END;
				pthread_cancel(inputthread);
			}
		break;
	}
}

void CarsGame::generate_blockcircle() {
	if(random.integer(1)) {	//Generate a block
		if (random.integer(1))
			leftblock.push_front(Block(5,1,maxY,random.integer(1)));
		else
			rightblock.push_front(Block(15,1,maxY,random.integer(1)));
	}
	else {			//Generate a circle
		if (random.integer(1))
			leftcircle.push_front(Circle(5,1,maxY,random.integer(1)));
		else
			rightcircle.push_front(Circle(15,1,maxY,random.integer(1)));
	}
}

void CarsGame::update_blockcircle() {
	for (int i = 0; i < leftblock.size(); i++) {
		gui << leftblock[i];
		leftblock[i].update();
		if (i == leftblock.size()-1 && leftblock[i].atmaxY()) {
			if (carA.getlane() == leftblock[i].getlane()) 
				gameover();
			leftblock.pop_back();
		}
	}
	for (int i = 0; i < leftcircle.size(); i++) {
		gui << leftcircle[i];
		leftcircle[i].update();
		if (i == leftcircle.size()-1 && leftcircle[i].atmaxY()) {
			if (carA.getlane() == leftcircle[i].getlane())
				score++;
			else 
				gameover();
			leftcircle.pop_back();
		}
	}
	for (int i = 0; i < rightblock.size(); i++) {
		gui << rightblock[i];
		rightblock[i].update();
		if (i == rightblock.size()-1 && rightblock[i].atmaxY()) {
			if (carB.getlane() == rightblock[i].getlane()) 
				gameover();					
			rightblock.pop_back();
		}
	}
	for (int i = 0; i < rightcircle.size(); i++) {
		gui << rightcircle[i];
		rightcircle[i].update();
		if (i == rightcircle.size()-1 && rightcircle[i].atmaxY()) {
			if (carB.getlane() == rightcircle[i].getlane())
				score++;
			else 
				gameover();						
			rightcircle.pop_back();
		}
	}
}

void CarsGame::gameover() {
	if (highscore.compare_score(score))
		highscore.set_score(score);					
	gamestate = GAMEOVER;
}
