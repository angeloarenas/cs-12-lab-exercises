#include "GUI.h"
#include <iostream>
#include <stdlib.h>
using namespace std;

void GUI::outputxy(char input,int x,int y) {
	cout << "\033[" << y << ';' << x << "H" << input;
}

void GUI::outputxy(string input,int x,int y) {
	cout << "\033[" << y << ';' << x << "H" << input;
}

void GUI::gotoxy(int x,int y) {
	cout << "\033[" << y << ';' << x << "H";
}


void GUI::clearscreen() {
	system("clear");
}

void GUI::hidecursor() {
	cout << "\033[?25l";
};

void GUI::showcursor() {
	cout << "\033[?25h";
};
