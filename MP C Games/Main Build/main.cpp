#include "./Snake/snakegame.h"
#include "./Cars/carsgame.h"
#include <iostream>
#include <stdio.h>
#include "GUI.h"
using namespace std;

int menu();
void snakeinstructions();
void carsinstructions();

int main() {
	GUI gui;
	gui.clearscreen();
	int ans = menu();	
	gui.clearscreen();
	if (ans == 1) {
		snakeinstructions();
		SnakeGame snakegame(80,30);
		snakegame.newGame();
	}
	else if (ans == 2) {
		carsinstructions();
		CarsGame carsgame;
		carsgame.newGame();
	}
	cout << "Program properly terminated\n";
}

int menu() {
	int choice;
	cout << "Which do you want to play? \n";
	cout << "1. Snake\n2. Cars\n";
	while(choice != 1 && choice != 2) {
		cout << "<Enter 1 or 2>: ";
		cin >> choice;
	}
	return choice;
}

void snakeinstructions() {
	cout << "Classic Snake Game\n";
	cout << "Avoid bumping into the borders and to the snake's body\n";
	cout << "Eat the food to gain score\n";
	cout << "CONTROLS: w a s d\np to pause\n";
	cout << "ENTER TO START GAME\n";
	cin.ignore(100,'\n');
	cin.get();
}

void carsinstructions() {
	cout << "Cars Game\n";
	cout << "From the popular 2 Cars game on iOS and Android\n";
	cout << "Avoid the 'X' and Collect all 'O' to gain score\n";
	cout << "CONTROLS: a d\np to pause\n";
	cout << "ENTER TO START GAME\n";
	cin.ignore(100,'\n');
	cin.get();
}
/*
Known bugs:
Fast press body hit or hold both key
	when key is pressed to change direction 
	and main thread hasn't updated the head position
	pressing another direction again may lead to body hit
	if head pos is updated upon change direction there will be double update
	because main thread will update it, putting a bool flag 
	is sort of dirty
	fix thread sync

If ctrl-c is invoked while hidecursor is on destructor wouldnt be able to 
run showcursor
*/

