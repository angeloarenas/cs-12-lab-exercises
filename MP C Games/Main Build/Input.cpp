#include <termios.h>
#include "Input.h"
#include <unistd.h>
#include <stdio.h>
#include "./Snake/snakegame.h"
#include "./Cars/carsgame.h"

void Input::autoin_on() {
	tcgetattr(STDIN_FILENO, &t); //get the current terminal I/O structure
	t.c_lflag &= ~ICANON; //Manipulate the flag bits to do what you want it to do
	t.c_lflag &= ~ECHO;
	tcsetattr(STDIN_FILENO, TCSANOW, &t); //Apply the new settings
}

void Input::autoin_off() {
	tcgetattr(STDIN_FILENO, &t); //get the current terminal I/O structure
	t.c_lflag |= ICANON; //Manipulate the flag bits to do what you want it to do
	t.c_lflag |= ECHO;
	tcsetattr(STDIN_FILENO, TCSANOW, &t); //Apply the new settings
}

//Should have created SnakeInput  as inheriting class of Input but since Input has
//few functions I've put it here.
void *Input::snakeinput(void *arg) {
	char s;
	while (true) {
		s = getchar();
		((SnakeGame *) arg) -> takeinput(s);
	}
	return NULL;
}

void *Input::carsinput(void *arg) {
	char s;
	while (true) {
		s = getchar();
		((CarsGame *) arg) -> takeinput(s);
	}
	return NULL;
}
