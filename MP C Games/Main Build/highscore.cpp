#ifndef HIGHSCORE_CPP
#define HIGHSCORE_CPP
#include "highscore.h"

template<typename T>
Highscore<T>::Highscore(string input)
	: filename(input) {
	fin.open(filename.c_str());
	if (!fin.fail())
		fin >> score;
	else
		score = 0;
	fin.close();
}

template<typename T>
Highscore<T>::~Highscore() {
	fin.close();
	fout.close();
}

template<typename T>
void Highscore<T>::set_score(T input) {
	score = input;
	fout.open(filename.c_str());
	fout << score;
	fout.close();
}


template<typename T>
T Highscore<T>::get_score() {
	return score;
}

template<typename T>
bool Highscore<T>::compare_score(T input) {
	if (input > score)
		return true;
	else 
		return false;
}

#endif
