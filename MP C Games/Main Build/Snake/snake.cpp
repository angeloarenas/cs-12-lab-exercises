#include "snake.h"

//for test
#include <iostream>

using namespace std;

Snake::Snake(int input_length, int inX, int inY)
	: snakechar('A'), direction(RIGHT),
	  mapdim_x(inX), mapdim_y(inY) {
	for (int  i = 0; i < input_length; i++) {
		bodyx.push_back(3);		//change according to mapx
		bodyy.push_back(i+3);
	}
}

GUI& operator<<(GUI& gui, Snake& snake) {
	for (int i = 0; i < snake.bodyx.size(); i++)
		gui.outputxy((char)(((int)'0')+i),snake.bodyx[i],snake.bodyy[i]);
}

void Snake::move() {
	for (int i = bodyy.size()-1; i >= 0; i--) {
		bodyx[i+1] = bodyx[i];
                bodyy[i+1] = bodyy[i];	
	}
	switch (direction) {
		case LEFT:
			bodyx[0]--;
		break;
		case RIGHT:	
			bodyx[0]++;
		break;
		case UP:
			bodyy[0]--;
		
		break;
		case DOWN:
			bodyy[0]++;
		break;
	}
	
}

void Snake::changedirection(Direction input) {
	switch (input) {
		case LEFT:
			if (direction != RIGHT && direction != LEFT) 
				direction = input;				
		break;
		case RIGHT:
			if (direction != RIGHT && direction != LEFT) 	
				direction = input;	
		break;
		case UP:
			if (direction != UP && direction != DOWN) 
				direction = input;	
		break;
		case DOWN:
			if (direction != UP && direction != DOWN)	
				direction = input;	
		break;
	}
}

bool Snake::eat(Food &food) {
	if(bodyx[0]==food.getx() && bodyy[0] == food.gety()) {
		grow();		
		return true;
	}
	return false;
}

void Snake::grow() {
	bodyx.push_back(bodyx[bodyx.size()-1]);
	bodyy.push_back(bodyy[bodyy.size()-1]);
}

bool Snake::bodyhit() {
	for (int i = 1; i < bodyx.size(); i++) {	
		if (bodyx[0] == bodyx[i] && bodyy[0] == bodyy[i]) {
			//cout << "BODY HIT";
			return true;
		}
	}
	return false;
}

bool Snake::borderhit() {
	if (bodyx[0] < 2 || bodyy[0] < 2 || bodyx[0] > mapdim_x-1 || bodyy[0] > mapdim_y-1) {
		//cout << "BORDER HIT";		
		return true;
	}
	return false;
}
