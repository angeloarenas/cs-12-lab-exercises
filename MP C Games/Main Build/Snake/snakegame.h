#include "../highscore.h"
#include "../highscore.cpp"	
#include "../GUI.h"
#include "snake.h"
#include <pthread.h>
#include "food.h"

class SnakeGame {
	enum GameState {PLAY, PAUSE, GAMEOVER, END};

	private:
		int mapX, mapY;
		int sleepTime;
		Highscore<int> highscore;
		GameState gamestate;
		GUI gui;
		Input input;
		pthread_t inputthread;

		Snake snake;
		Food food;

		int score;
	public:
		SnakeGame(int,int);
		~SnakeGame();
		void newGame();
		void gameLoop();
		void playLoop();
		void pauseLoop();
		void gameoverLoop();

		void takeinput(char);
		void start_inputthread();
		void print_map();
};

/*
highscore.cpp must be included because template class cannot be in separate files
*/
