#include "snakegame.h"
#include <unistd.h>
#include <iostream>	//test

using namespace std;

SnakeGame::SnakeGame(int inX, int inY)
	: mapX(inX), mapY(inY), snake(5,inX,inY), 
	score(0), sleepTime(55000),
	highscore("snakehighscore.txt") {
	
}

SnakeGame::~SnakeGame() {
	gui.clearscreen();
	gui.showcursor();
	input.autoin_off();
	cout << "SnakeGame destructor called\n";
}

void SnakeGame::newGame() {
	gamestate = PLAY;
	gui.hidecursor();
	input.autoin_on();
	start_inputthread();
	gameLoop();
	pthread_join(inputthread,NULL);
}

void SnakeGame::gameLoop() {
	char key;
	food.generate();
	score = 0;
	while(gamestate!=END) {		
		switch (gamestate) {
			case PLAY:
				playLoop();
			break;
			case PAUSE:
				pauseLoop();
			break;
			case GAMEOVER: 
				gameoverLoop();
			break;
		}
		cout.flush(); //FOR SLEEP
		usleep(sleepTime);
	}
}

void SnakeGame::playLoop() {
	if (snake.bodyhit() || snake.borderhit()) {
			if (highscore.compare_score(score))
				highscore.set_score(score);
			gamestate = GAMEOVER;
	}
	gui.clearscreen();
	gui.gotoxy(mapX/2-9,mapY+3);
	cout << "Score: " << score;
	gui << snake;
	gui << food;
	print_map();
	if (snake.eat(food)) {
		food.generate();
		score++;
	}
	snake.move();
}

void SnakeGame::pauseLoop() {
	gui.outputxy("P A U S E D",mapX/2-9,mapY/2-1);
	gui.outputxy("p to UNPAUSE",mapX/2-13,mapY/2);
	gui.outputxy("e to EXIT",mapX/2-13,mapY/2+1);
}

void SnakeGame::gameoverLoop() {
	gui.outputxy("GAMEOVER",mapX/2-9,mapY/2-2);
	gui.gotoxy(mapX/2-9,mapY/2-1);
	cout << "Your score: " << score;
	gui.gotoxy(mapX/2-9,mapY/2);
	cout << "Highsore: " << (int)highscore.get_score();
	gui.outputxy("e to EXIT",mapX/2-9,mapY/2+1);	
}

void SnakeGame::takeinput(char input) {
	switch(input) {
		case 'w':
			snake.changedirection(snake.UP);
		break;	
		case 'a':
			snake.changedirection(snake.LEFT);
		break;		
		case 's':
			snake.changedirection(snake.DOWN);
		break;
		case 'd':
			snake.changedirection(snake.RIGHT);
		break;
		case 'p':
			if (gamestate != GAMEOVER)
				gamestate==PLAY?gamestate=PAUSE:gamestate=PLAY;
		break;
		case 'e':
			if (gamestate == PAUSE || gamestate == GAMEOVER) {
				gamestate = END;
				pthread_cancel(inputthread);
			}
		break;
	}
}

void SnakeGame::start_inputthread() {
	pthread_create(&inputthread,NULL,input.snakeinput,this);
}

void SnakeGame::print_map() {
	for (int i = 2; i < mapX; i++) {
		gui.outputxy('_',i,0);
		gui.outputxy('_',i,mapY);
	}
	for (int i = 2; i < mapY+1; i++) {
		gui.outputxy('|',1,i);
		gui.outputxy('|',mapX,i);
	}
};
