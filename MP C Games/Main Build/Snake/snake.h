#include <vector>
#include "../GUI.h"
#include "../Input.h"
#include "food.h"
using namespace std;

class Snake {
	friend GUI& operator<<(GUI&,Snake&);
	public: enum Direction {LEFT,RIGHT,UP,DOWN};
	private:
		char snakechar;
		int mapdim_x, mapdim_y;
		vector<int> bodyx;
		vector<int> bodyy;
		Direction direction;
	public:
		Snake(int,int,int);
		void move();
		void changedirection(Direction);
		bool eat(Food&);
		void grow();
		bool bodyhit();
		bool borderhit();
};
