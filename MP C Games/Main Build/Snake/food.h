#ifndef FOOD_H
#define FOOD_H

#include "../randgen.h"
#include "../GUI.h"

class Food {
	friend GUI& operator<<(GUI&,Food&);
	private:
		Random rand;	
				
		char foodchar;	
		int x, y;
		int min_x;
		int max_x;
		int min_y;
		int max_y;
	public:
		Food(char='A',int=2,int=25,int=2,int=25);
		void generate();
		int getx();
		int gety();
};

#endif
