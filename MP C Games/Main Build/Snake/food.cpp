#include "food.h"

Food::Food(char inputchar, int minx, int maxx, int miny, int maxy)
: foodchar(inputchar), min_x(minx), max_x(maxx), min_y(miny), max_y(maxy) {
}

void Food::generate() {
	x = rand.integer(max_x,min_x);	
	y = rand.integer(max_y,min_y);
}

GUI& operator<<(GUI& gui, Food& food) {
	gui.outputxy(food.foodchar,food.x,food.y);
}

int Food::getx() {
	return x;
}

int Food::gety() {
	return y;
}
