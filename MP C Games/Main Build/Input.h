#ifndef INPUT_H
#define INPUT_H

#include <termios.h>
class Input {
	private:
		struct termios t;
	public:
		void autoin_on();	//Buffered input
		void autoin_off();
		static void *snakeinput(void *arg);	
		static void *carsinput(void *arg);
};

/*
credits to:
http://www.cplusplus.com/forum/general/29137/
Luke Leber
*/


#endif
