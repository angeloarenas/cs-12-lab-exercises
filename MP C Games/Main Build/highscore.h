#ifndef HIGHSCORE_H
#define HIGHSCORE_H

#include <string>
#include <fstream>

using namespace std;

template <typename T>
class Highscore {
	private:
		ofstream fout;
		ifstream fin; 
		string filename;
		T score;
		string alias;
	public:
		Highscore(string);
		~Highscore();
		void set_score(T);
		T get_score();
		bool compare_score(T);
};

#endif
