/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
Lab Exercise 4: Fractions
*/

#include <iostream>

using namespace std;
class Frac {
	friend istream& operator>>(istream&, Frac&);
	friend ostream& operator<<(ostream&, const Frac&);

	friend Frac operator+(Frac&,Frac&);
	friend Frac operator-(Frac&,Frac&);
	friend Frac operator*(Frac&,Frac&);
	friend Frac operator/(Frac&,Frac&);

	public:
		Frac();
		Frac(int,int);
		
	private:
		int num, den;
		int gcf(int,int);
		void invariant();
		void simplify();
};


