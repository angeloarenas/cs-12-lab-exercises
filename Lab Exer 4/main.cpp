/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
Lab Exercise 4: Fractions
*/

#include <iostream>
#include "frac.h"

using namespace std;

int menu();

int main() {
	Frac frac, frac2, frac3;
	cout << "Enter fraction 1 and fraction 2: ";
	cin >> frac >> frac2;
	cout << "Fraction 1: " << frac << endl
		<< "Fraction 2: " << frac2 << endl;
	while(true)
		switch(menu()) {
			case 1:
				frac3 = frac+frac2;
				cout << "Sum: "<< frac3 << endl;
				break;
			case 2:
				frac3 = frac-frac2;
				cout << "Difference: "<< frac3 << endl;
				break;
			case 3:
				frac3 = frac*frac2;
				cout << "Product: "<< frac3 << endl;
				break;
			case 4:
				frac3 = frac/frac2;
				cout << "Quotient: "<< frac3 << endl;
				break; 
		}
	;
	
}

int menu() {
	cout << "\n\nChoose operation: " << endl;
	cout << "1. Addition " << endl;
	cout << "2. Subtraction " << endl;
	cout << "3. Multiplication " << endl;
	cout << "4. Division " << endl;
	cout << "Ctrl-c to exit" << endl;
	cout << "Enter number: ";
	int ans;
	cin >> ans;
	return ans;
}
