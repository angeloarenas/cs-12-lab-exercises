/*
Arenas, Angelo V.
BS CS - 2014-89285
CS12 WFXY
Prof. Jaymar Soriano
Lab Exercise 4: Fractions
*/

#include "frac.h"
#include <stdlib.h>
#include <iostream>

using namespace std;

istream& operator>>(istream& in, Frac& frac) {
	char temp;
	in >> frac.num;
	if (in.peek() == '/') {
		in >> temp;
		in >> frac.den;
	}
	else
		frac.den = 1;
	frac.invariant();
	return in;
}

ostream& operator<<(ostream& out, const Frac& frac) {
	out << frac.num << "/" << frac.den;
	return out;
}

Frac operator+(Frac& frac, Frac& frac2) {
	int num, den;
	if (frac.den == frac2.den) {
		num = frac.num + frac2.num;
		den = frac.den;
	}
	else {
		num = (frac.num*frac2.den) + (frac2.num*frac.den);
		den = frac.den*frac2.den;
	}

	return Frac(num,den);
}

Frac operator-(Frac& frac, Frac& frac2) {
	int num, den;
	if (frac.den == frac2.den) {
		num = frac.num - frac2.num;
		den = frac.den;
	}
	else {
		num = (frac.num*frac2.den) - (frac2.num*frac.den);
		den = frac.den*frac2.den;
	}

	return Frac(num,den);
}

Frac operator*(Frac& frac, Frac& frac2) {
	int num, den;
	num = frac.num*frac2.num;
	den = frac.den*frac2.den;

	return Frac(num,den);
}

Frac operator/(Frac& frac, Frac& frac2) {
	int num, den;
	num = frac.num * frac2.den;
	den = frac.den * frac2.num;
	
	return Frac(num,den);
}

Frac::Frac() {}

Frac::Frac(int in_num, int in_den) 
	: num(in_num), den(in_den) {
	invariant();
}

int Frac::gcf(int u, int v) {
	//Credits to: http://rosettacode.org/wiki/Greatest_common_divisor#C.2FC.2B.2B
	int t;
	while (v) {
		t = u; 
		u = v; 
		v = t % v;
	}
  	return u < 0 ? -u : u;
}

void Frac::simplify() {
	int factor = gcf(num,den);
	num/=factor;
	den/=factor;
}

void Frac::invariant() {	
	if (den != 0)	{
		simplify();
	}
	else {
		cout << "INVALID OPERATION, DENOMINATOR = 0" << endl;	
		exit(0);
	}
}
