#include <vector>
#include "ClassRecord.h"

class Database {
	private:
		vector<ClassRecord> classrecords;	//Vector containing all ClassRecord objects

	public:
		void createClassRecord(); 
			/* Adds new ClassRecord object to classrecords vector 
				will run DC or PC of ClassRecord then run 
				ClassRecord::input_studentdata()
				ClassRecord::input_classdata() 
				ClassRecord::save_classdata()
				ClassRecord::save_studentdata() */
		void updateClassRecord(int); 
			/* Modify ClassRecord object from vector with id given by the input int 
			will print submenu then run function that is chosen
			for example:
				classrecords(i).print_passed()
			ClassRecord::input_classdata() to add grade requirement
			ClassRecord::modify_requirementpercentages(int) to modify req/perc 
			ClassRecord::input_studentgrades() to input grades
			ClassRecord::compute_finalgrade() to compute final grades
			ClassRecord::print_classrecord() to print class record
			ClassRecord::print_passed() to print students who passed
			ClassRecord::print_failed() to print students who failed
			ClassRecord::access_student(string) to access student by student no
			*/
		void printClassRecords(); 
			/* Prints all data from ClassRecord objects of classrecords vector */
};

/*
Database has a ClassRecord
ClassRecord has a Student
*/
