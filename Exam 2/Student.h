#include <vector>
#include <string>
using namespace std;

class Student {
	private: 
		string last_name;
		string first_name;
		string student_no;
		vector<double> grades; //Vector containing all grades/percentages
		double finalgrade;
		bool passfail;		
	public:
		bool isPassed();
			/* returns value of passfail */
		void print_record();
			/* prints all data including grades vector */
		void modify_grade(int);
			/* modifies value of grades[inputint] */
		void input_grades();
			/* will ask for values for all values in grades[] */
		void compute_grades();
			/* will compute for finalgrade then determine value for passfail */
};
