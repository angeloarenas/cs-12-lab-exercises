#include <string>
#include <vector>
#include "Student.h"

using namespace std;

class ClassRecord {
	private:
		string subjectname;
		string schedule;
		int totalstudents;
		Student* students;		//Students array
		vector<string> requirement;	//Vector from STL parallel to percentages
		vector<double> percentages;	//Vector from STL parallel to requirements
	public:
		ClassRecord();		
			/* DC-will ask for subject name, schedule and number of students and will create students array based on number of students*/
		ClassRecord(string, string, int);
			/* PC-will take subject name, schedule and number of students and will create students array based on number of students*/
		void input_studentdata();
			/* will loop through all elements in students[] and ask for Lname, Fname and Student No.*/
		void input_classdata();
			/* will ask input for requirement and percentage which will be added to both parallel vectors */
		void input_studentgrades();
			/* will loop through all students[] and run Student::input_grades() on each */
		void modify_requirementpercentages(int);
			/* will take in element number of which requirement/percentage to be changed then will ask user the changes to be made */
		void compute_finalgrade();
			/* will run Student::compute_grades() for all objects in students[] */
		void access_student(string);
			/* will access student with the same student_no as the input string, prints student record, and modifies grade entries through Student::modify_grade(int) */
		void save_studentdata(string = "students.txt");
			/* will save student data to file */
		void save_classdata(string = "class.txt");
			/* will save class data to file */
		void print_classrecord();
			/* will output all needed data in this object */
		void print_passed();
			/* will output all objects in students[] that has isPassed=true */
		void print_failed();
			/* will output all objects in students[] that has isPassed=false */
};
