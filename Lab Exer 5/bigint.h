/*
Arenas, Angelo V.
201489285
CS 12 WFXY
Prof. Jaymar Soriano
*/
#include <string>
using namespace std;
class BigInt {
	private:
		string number;
		void invariance();
		int char_to_int(char);
		char int_to_char(int);
	
	public:
		BigInt();
		BigInt(const char[]);
		BigInt(long long int);

		BigInt factorial();
		bool isPrime();

	friend BigInt operator++(BigInt);
	friend BigInt power(BigInt, BigInt);	
	friend BigInt operator+(BigInt, BigInt);
	friend BigInt operator-(BigInt, BigInt);
	friend BigInt operator*(BigInt, BigInt);
	friend BigInt operator%(BigInt, BigInt);

	friend bool operator>(BigInt, BigInt);
	friend bool operator<(BigInt, BigInt);
	friend bool operator==(BigInt, BigInt);

	friend istream& operator>>(istream&, BigInt&);
	friend ostream& operator<<(ostream&, const BigInt&);
};
