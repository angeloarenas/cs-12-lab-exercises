/*
Arenas, Angelo V.
201489285
CS 12 WFXY
Prof. Jaymar Soriano
*/
#include <iostream>
#include <string>
#include "bigint.h"

using namespace std;

int main(int argv, char* args[]) {
	BigInt a, b;
	int ans;
	while (true) {
	cout << "Enter option: ";
	cout << "\n1. Addition\n"
	 << "2. Subtraction\n"
	 << "3. Multiplication\n"
	 << "4. Modulo\n"
	 << "5. Factorial\n"
	 << "6. Prime Number Test\n"
	 << "Ctrl-c to quit\n";
	cin >> ans;
	switch(ans) {
		case 1:
			cout << "Enter two numbers: ";
			cin >> a >> b;
			cout << "SUM: " << a + b;
		break;
		case 2:
			cout << "Enter two numbers: ";
			cin >> a >> b;
			cout << "DIFFERENCE: " << a - b;
		break;
		case 3:
			cout << "Enter two numbers: ";
			cin >> a >> b;
			cout << "PRODUCT: " << a * b;
		break;
		case 4:
			cout << "Enter two numbers: ";
			cin >> a >> b;
			cout << "MODULO: " << a % b;
		break;
		case 5:
			cout << "Enter a numbers: ";
			cin >> a;
			cout << "FACTORIAL: " << a.factorial();
		break;
		case 6:
			cout << "Enter a numbers: ";
			cin >> a;
			a.isPrime()? cout << "PRIME\n" : cout << "NOT PRIME\n";
		break;			
	}
	}
}

