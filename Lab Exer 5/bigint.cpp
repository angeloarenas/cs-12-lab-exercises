/*
Arenas, Angelo V.
201489285
CS 12 WFXY
Prof. Jaymar Soriano
*/

#include <iostream>
#include <string>
#include "bigint.h"

using namespace std;

BigInt::BigInt() {
	number = "0";
}

BigInt::BigInt(long long int input) {
	number = "";
	while(input > 0) {
		number.insert(0,1,int_to_char(input%10));
		input /= 10;
	}
	//cout << number << " " << number.size() << endl;
}

BigInt::BigInt(const char input[]) {
	number = input;
	invariance();
	//cout << number << " " << number.size() << endl;
}

void BigInt::invariance() {
	for (int i = 0; i < number.size(); i++) {
		if (number[i]-'0' < 0 || number[i]-'9'>0) {
			cout << "\nCharacter present in the string\n";
			number.erase(i,1);
		}
	}
	for (int i = 0; i < number.size(); i++) {
		if (char_to_int(number[i]) == 0) {
			number.erase(number.begin()+i);
			i--;
		}
		else 	
			break;
	}
}

BigInt operator+(BigInt a, BigInt b) {
	int carry = 0;
	int iterations;	
	int sum = 0;	
	a.number.size()>=b.number.size()? iterations = a.number.size(): iterations = b.number.size();
	BigInt bigsum;
	bigsum.number="";
	for (int i = 0; i < iterations; i++) { 
		sum = 0;
		if (i < a.number.size())
			sum += a.char_to_int(a.number[a.number.size()-1-i]);
		if (i < b.number.size())
			sum += b.char_to_int(b.number[b.number.size()-1-i]);
		sum += carry;
		carry = 0;
		if (sum >= 10) {
			carry++;
			sum = sum%10;
		}
		bigsum.number.insert(0,1,bigsum.int_to_char(sum));
	}
	if (carry > 0)
		bigsum.number.insert(0,1,bigsum.int_to_char(carry));
	bigsum.invariance();
	return bigsum;
}

BigInt operator-(BigInt a, BigInt b) {
	int borrow = 0;
	int iterations;	
	int diff = 0;
	a.number.size()>=b.number.size()? iterations = a.number.size(): iterations = b.number.size();
	string output="";
	
	for (int i = 0; i < iterations; i++) { 
		diff = 0;
		if (i < a.number.size())
			diff = a.char_to_int(a.number[a.number.size()-1-i]);
		if (i < b.number.size())
			diff -= b.char_to_int(b.number[b.number.size()-1-i]);
		diff -= borrow;
		borrow = 0;
		if (diff < 0) {
			diff += 10;
			borrow++;
		}
		output.insert(0,1,a.int_to_char(diff));
	}
	BigInt bigoutput;
	bigoutput.number = output;
	bigoutput.invariance();
	//cout << bigoutput;
	return bigoutput;
}

BigInt operator*(BigInt a, BigInt b) {
	int iterations;	
	string output = "";	
	BigInt product, bigproduct;
	iterations = b.number.size();
	for (int i = 0; i < iterations; i++) { 
		for (int j = 0; j < b.char_to_int(b.number[b.number.size()-1-i]); j++) 
			product = product + a;
		product.number.insert(product.number.size(),i,'0');
		bigproduct = bigproduct + product;
		product.number = "0";
	}
	bigproduct.invariance();
	return bigproduct;
}

BigInt operator%(BigInt a, BigInt b) {
	//Iterative difference
	BigInt modulo;
	modulo.number = a.number;
	//modulo=modulo-b;
	while(modulo > b || modulo == b) {
		modulo=modulo-b;	
	}
	modulo.invariance();
	return modulo;
}

bool operator>(BigInt a, BigInt b) {
	if (a.number.size() == b.number.size()) {
		for (int i = 0; i < a.number.size(); i++) {
			if (a.number[i] > b.number[i])
				return true;
			else if (a.number[i] < b.number[i])
				return false;
		}
	}
	if (a.number.size() > b.number.size())
		return true;
	else
		return false;
	return false;
}

bool operator<(BigInt a, BigInt b) {
	if (a.number.size() == b.number.size()) {
		for (int i = 0; i < a.number.size(); i++) {
			if (a.number[i] < b.number[i])
				return true;
			else if (a.number[i] > b.number[i])
				return false;
		}
	}
	if (a.number.size() < b.number.size())
		return true;
	else
		return false;
	return false;
}

bool operator==(BigInt a, BigInt b) {	
	if (a.number.size() != b.number.size())
		return false;
	else {
		for (int i = 0; i < a.number.size(); i++) {
			if (a.number[i] != b.number[i])
				return false;	
		}
		return true;
	}	
}

BigInt operator++(BigInt add) {
	return (1+add.number.c_str());
}

BigInt BigInt::factorial() {	
	BigInt fact(number.c_str());
	BigInt multiplier = fact-1;
	while(multiplier > 1) {
		fact=fact*multiplier;
		multiplier = multiplier-1;
	}
	return fact;
}

BigInt power(BigInt a, BigInt b) {
	BigInt pow = a;
	while(b>1) {
		pow = pow*a;
		b = b-1;
	}
	return pow;
}

bool BigInt::isPrime() {
	BigInt bigInt(number.c_str());
	for (BigInt i = 2; i < power(10,number.size()-number.size()/2) ; i=i+1) {
		if (bigInt%i == "0") {
			cout << "Divisible by: " << i;
			return false;
		}
	}
	return true;
}

ostream& operator<<(ostream& out, const BigInt& bigint) {
	out << bigint.number << endl;
	return out;
}

istream& operator>>(istream& in, BigInt& bigint) {
	in >> bigint.number;
	bigint.invariance();
	return in;
}

int BigInt::char_to_int(char in) {
	return (in-'0');
}

char BigInt::int_to_char(int input) {
	char conv = input + '0';
	return conv;
}
