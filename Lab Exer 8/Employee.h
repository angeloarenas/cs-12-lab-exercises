class Employee { 
	private: 
		string Name;
		double netSalary;
	public:
		void enterSalaryDetails();
		double computeSalary();
		void printPaySlip();
};
