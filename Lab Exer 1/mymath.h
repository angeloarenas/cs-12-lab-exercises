/*
Arenas, Angelo V.
201489285
CS 12 WFXY
Prof. Jaymar Soriano
*/


double power(double, int);
double sqRoot(double);
bool isPrime(int);
void primesLessThan(int);
double sine(double);
double cosine(double);
double exp(double);
double loge(double);
double power(double, double);
void universalconstants();
