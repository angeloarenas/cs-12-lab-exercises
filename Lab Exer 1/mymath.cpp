/*
Arenas, Angelo V.
201489285
CS 12 WFXY
Prof. Jaymar Soriano
*/

#include "mymath.h"
#include <iostream> 		
#include <iomanip>

using namespace std;			

//Universal Constants Variables
double const pi = 3.1415926536;
double const e = 2.7182818285;
double const phi = 1.6180339887;
double const c = 299792458;
double const G = 6.6738e-11;
double const h = 6.6261e-34;

double power(double a, int b) {
	//Assuming b is positive integer as stated in (1)
	if (b == 0)
		return 1;
	else if (b == 1)
		return a;
	else if (b%2 == 0) {
		//cout << "even";
		return power(a*a, b/2);
	}
	else if (b%2 == 1) {
		//cout << "odd";
		return a*power(a*a,b/2);
 	}
}

double sqRoot(double a) {
	if (a < 0) {
		cout << "INVALID OPERATION\n";
		return 0;
	}	
	double x0 = a/2;
	double x1;
	double difference;
	do {
		x1 = (x0+(a/x0))/2;
		difference = x0-x1;
		x0 = x1;
			
	} while (difference > 0.000001);
	return x1;
}

bool isPrime(int n) {
	for (int i = 2; i <= (int)sqRoot(n)+1; i++) {
		if (n%i == 0) {
			//cout << "Divisible by: " << i << endl;
			return false;
		}
	}
	return true;
}

void primesLessThan(int n) {
	bool array[n];
	//init
	for (int i = 0; i < n; i++) {
		array[i] = true;
	}
	//proc
	for (int i = 2; i < n; i++) {
		if (array[i] == true) {
			//if (!isPrime(i)) 
			//	array[i] = false;
			for (int j = i; j <= n/i; j++) {
				array[i*j] = false;
			}
		}
	}
	//disp
	for (int i = 2; i < n; i++) {
		if (array[i] == true) {
			cout << i << " ";
		}
	}
	cout << endl;
}

double sine(double x) {
	if (x < 0.000001 && x > -0.000001)
		return (x - (power(x,3)/6) + (power(x,5)/120));
	else
		return (2*sine(x/2)*cosine(x/2));
}

double cosine(double x) {
	if (x < 0.000001 && x > -0.000001)
		return (1 - (power(x,2)/2) + (power(x,4)/24));
	else
		return ((2*power(cosine(x/2),2))-1);
}

double exp(double x) {
	if (x < 0.000001 && x > -0.000001)
		return (1 + x + power(x,2)/2 + power(x,3)/6);
	else
		return power(exp(x/10),10);
}

double loge(double a) {
	double difference;
	double x0, x1;
	if (a == 1)
		return 0;
	else {
		x0 = a/3;
		do {
			x1 = x0 - 1 + (a*exp(-x0));
			difference = x0-x1;
			x0 = x1;
		} while(difference > 0.000001 || difference < -0.000001);
		return x1;
	}
	
}

double power(double a, double b) {
	return (exp(b*loge(a)));
}

void universalconstants() {
	cout << "Universal Constants\n";
	cout << "π" << setw(20) << setfill('.') << setprecision(11) << pi << endl;
	cout << "e" << setw(20) << setfill('.') << setprecision(11) << e << endl;
	cout << "ф" << setw(20) << setfill('.') << setprecision(11) << phi << endl;
	cout << "c" << setw(20) << setfill('.') << c << endl;
	cout << "G" << setw(20) << setfill('.') << setprecision(5) << G << endl;
	cout << "h" << setw(20) << setfill('.') << setprecision(5) << h << endl;
}
