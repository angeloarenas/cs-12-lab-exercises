/*
Arenas, Angelo V.
201489285
CS 12 WFXY
Prof. Jaymar Soriano
*/

#include <iostream>
#include "mymath.h"

using namespace std;

void printmenu();

int main() {
	int option;
	do {	
		printmenu();
		cin>>option;
		switch(option) {
			case 1: {
				double a; int b;
				cout << "Input arguments <double> <int>: ";
				cin >> a >> b;
				cout << "Answer is: " << power(a,b) << "\n";
			}
			break;
			case 2: { 
				double a;
				cout << "Input argument <double>: ";
				cin >> a;
				cout << "Answer is: " << sqRoot(a) << "\n";
			}
			break;
			case 3: { 
				int n;
				cout << "Input argument <int>: ";
				cin >> n;
				cout << "Answer is: ";
				isPrime(n) ? cout << "Prime\n" : cout << 						"Not Prime\n";
			}
			break;
			case 4: { 
				int n;
				cout << "Input arguments <int>: ";
				cin >> n;
				primesLessThan(n);
			}
			break;
			case 5: { 
				double x;
				cout << "Operating in radians\n";
				cout << "Input argument <double>: ";
				cin >> x;
				cout << "Answer is: " << sine(x) << "\n";
			}
			break;
			case 6: { 
				double x;
				cout << "Operating in radians\n";
				cout << "Input argument <double>: ";
				cin >> x;
				cout << "Answer is: " << cosine(x) << "\n";
			}
			break;
			case 7: {
				double x;
				cout << "Input argument <double>: ";
				cin >> x;
				cout << "Answer is: " << exp(x) << "\n";
			}
			break;
			case 8: {
				double a;
				cout << "Input argument <double>: ";
				cin >> a;
				cout << "Answer is: " << loge(a) << "\n";
			}
			break;	
			case 9: {
				double a, b;
				cout << "Input argument <double> <double>: ";
				cin >> a >> b;
				cout << "Answer is: " << power(a,b) << "\n";
			}
			break;
			case 10: {
				universalconstants();
			}
			break;
		}
	} while(option != 11);
}

void printmenu() {
	cout << "1. power(double a,int b) \n";
	cout << "2. sqRoot(a) \n";
	cout << "3. isPrime(n) \n";
	cout << "4. primesLessThan(n) \n";
	cout << "5. sine(x) \n";
	cout << "6. cosine(x) \n";
	cout << "7. exp(x) \n";
	cout << "8. loge(a) \n";
	cout << "9. power(a,b) \n";
	cout << "10. Universal Constants \n";
	cout << "11. Exit \n";
}
