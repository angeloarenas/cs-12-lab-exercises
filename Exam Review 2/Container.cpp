#include <iostream>

using namespace std;

template <typename t> 
class Vector {
	int size;
	int count;
	t* data;
	public:
	void push_back(t);
	void init_vector(int);
	t at(int);
};

template <typename t>
void Vector<t>::init_vector(int size) {
	data = new t[size];
	count = 0;
}

template <typename t>
void Vector<t>::push_back(t input) {
	data[count] = input;
	count++;
}

template <typename t>
t Vector<t>::at(int id) {
	return data[id];
}

int main() {
	Vector<int> myvector;
	myvector.init_vector(5);
	myvector.push_back(20);
	myvector.push_back(20);
	myvector.push_back(20);
myvector.push_back(20);
	cout << myvector.at(0);
	Vector<Vector<int> > doublevector;
	doublevector.push_back(myvector);
}
