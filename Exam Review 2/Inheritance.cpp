#include <iostream>
using namespace std;

class B {
	int x;
	public:
		B():x(10) {};
		void display() {
			cout << "x = " << x << endl;
		}
		virtual void disp() = 0;
		virtual void asdf() = 0;
};

class D: public B {
	int x;
	public:
		D() : x(20) {};
		void display() {cout << "x = " << x << endl;}
		virtual void disp();
		virtual void asdf();
};

void D::disp() {
	cout << "adsf";
}

void D::asdf() {
	cout << "adsf";
}

int main() {
	D derived;
	derived.display();
	//derived.B::display();
	derived.disp();
}
