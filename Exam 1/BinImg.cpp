/*
Arenas, Angelo V.
201489285
CS 12 WFXY
Prof. Jaymar Soriano
Exam 1 - binary image
*/

#include <string>
#include <fstream>
#include "BinImg.h"

BinImg::BinImg() {
}

BinImg::BinImg(string filename) {
	inputfile.open(filename.c_str());
	decode();
}

BinImg::BinImg(const BinImg& a) {
	sizey = a.sizey;
	sizex = a.sizex;
	init(sizey,sizex);
	for (int i = 0; i < sizey; i++) {
		for (int j = 0; j < sizex; j++) {
			image[i][j] = a.image[i][j];
		}
	}
}

BinImg& BinImg::operator=(const BinImg& a) {
	sizey = a.sizey;
	sizex = a.sizex;
	init(sizey,sizex);
	for (int i = 0; i < sizey; i++) {
		for (int j = 0; j < sizex; j++) {
			image[i][j] = a.image[i][j];
		}
	}
	return *this;
}

ostream& operator<<(ostream& out, const BinImg& binimg) {
	for (int i = 0; i < binimg.sizey; i++) {
		for (int j = 0; j < binimg.sizex; j++) {
			cout << binimg.image[i][j];
		}
		cout << endl;
	}
	return out;
}

BinImg BinImg::operator!() {
	BinImg b;
	b.init(sizey,sizex);
	for (int i = 0; i < sizey; i++) {
		for (int j = 0; j < sizex; j++) {
			b.image[i][j] = !image[i][j];			
		}
	}
	return b;
}

BinImg operator&(const BinImg& a, const BinImg& b) {
	//a and b must have equal size
	if (a.sizex != b.sizex || a.sizey != b.sizey) {
		cout << "Error! Size must be equal\n";
		return a;
	}

	BinImg c;
	c.init(a.sizey,a.sizex);
	for (int i = 0; i < a.sizey; i++) {
		for (int j = 0; j < a.sizex; j++) {
			if (a.image[i][j] && b.image[i][j])	//intersection (|| for union)
				c.image[i][j] = true;
			else
				c.image[i][j] = false;				
		}
	}
	return c;
}

void BinImg::init(int inY, int inX) {
	sizey = inY;
	sizex = inX;
	
	image = new bool*[sizey];
	for (int i = 0; i < sizey; i++)
		image[i] = new bool[sizex];
}

void BinImg::decode() {
	int a = getbit();
	int b = getbit();
	init(a,b);
	
	bool turn;	//true for 1, false for 0
	int x = 0;
	int y = 0;
	while(y < sizey) {
		turn = true;
		while (x < sizex) {
			int intbit = getbit();
			for (int i = 0; i < intbit; i++) 
				image[y][x+i] = turn;
			x += intbit;
			turn = !turn;
		}
		y++;
		x=0;
	}

}

void BinImg::save(string filename) {
	ofstream fout(filename.c_str());
	fout << sizey << ',' << sizex;
	bool turn;
	int counter = 0;
	for (int i = 0; i < sizey; i++) {
		turn = true;	
		for (int j = 0; j < sizex; j++) {
			if (image[i][j] == turn)
				counter++;
			else {
				fout  << ',' << counter;
				counter = 1;
				turn = !turn;
			}
		}
		fout  << ',' << counter;
		counter = 0;
	}
}

int BinImg::getbit() {
	char bit;
	int intbit;
	while(true) {
		if(inputfile.peek() == ',') {
			inputfile.get();	
		}
		else {
			inputfile.get(bit);
			//cout << bit;
			intbit = bit-'0';
			break;		
		}
	}
	return intbit;
}

BinImg::~BinImg() {
	for (int i = 0; i < sizey; i++)
		delete[] image[i];
	delete[] image;
	inputfile.close();
}
