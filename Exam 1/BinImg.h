/*
Arenas, Angelo V.
201489285
CS 12 WFXY
Prof. Jaymar Soriano
Exam 1 - binary image
*/

#include <string>
#include <iostream>
#include <fstream>

using namespace std;
class BinImg {

	friend ostream& operator<<(ostream&, const BinImg&);	
	friend BinImg operator&(const BinImg&, const BinImg&); 	
	
	private:
		ifstream inputfile;
		bool** image;
		int sizex, sizey;

		int getbit();
		void decode();
		void init(int, int);

	public:	
		BinImg();
		BinImg(string);
		BinImg(const BinImg&);
		~BinImg();
		BinImg& operator=(const BinImg&);
		BinImg operator!();
		void save (string filename = "save.binx");
};
