/*
Arenas, Angelo V.
201489285
CS 12 WFXY
Prof. Jaymar Soriano
Exam 1 - binary image
*/

#include <iostream>
#include "BinImg.h"

using namespace std;

int main() {
	BinImg img1("S.binx");
	BinImg img2("2.binx");
	BinImg img3,img4;
	img3 = (!img1);
	img4=img1&img2;
	cout << "img1" << '\n' << img1 << '\n';
	cout << "img2" << '\n' << img2 << '\n';
	cout << "img3 (!img1) (reverse)" << '\n' << img3 << '\n';
	cout << "img4 (img1&img2) (intersection)" << '\n' << img4 << '\n';
	img3.save();
	cout << "img3 saved\n";
}
